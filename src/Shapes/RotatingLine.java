package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.vector2d;

public class RotatingLine extends Shape
{
	private vector2d c, p;         // points of the line
	private vector2d n;            // normal of the line
	private double l;               // length of the line
	private double angle;           // angle of the line
	private double rot = 0;         // amount the line should rotate this frame
	private double inertia = 30.0d; // moment of inertia scaler
	
	private double lineCoefficient;     // coefficient of the point of collision along the line of collision
	private vector2d pInitial, pFinal; // initial and final end-point positions of the line
	private vector2d nInitial, nFinal;           // final normal of the line
	private vector2d pUpper, pLower;   // upper and lower end-point positions of the line
	private vector2d nUpper, nLower;   // upper and lower normals of the line
	
	public RotatingLine(int x, int y, double x2, double y2, Part owner)
	{
		this.c = new vector2d(x, y);
		this.p = new vector2d(x2, y2);
		
		l = Math.sqrt((x2 - x)*(x2 - x) + (y2 - y)*(y2 - y));
		angle = Math.atan2(y2 - y, x2 - x);
		
		n = p.getNormal(c);
		
		setBoundaries(x - (int)l, y - (int)l, x + (int)l, y + (int)l);
		setOwner(owner);
	}

	//*********************************************************
	// Rotate
	//
	//  sets the amount the line should rotate this frame. Must
	//   be -90 < t < 90
	//*********************************************************
	public void Rotate(double t)
	{
		rot = t;
	}
	
	//*********************************************************
	// Post Check
	//
	//  after the collision check, update variables
	//*********************************************************
	public void PostCheck()
	{
		angle += rot;
		rot = 0;
		
		p.set(c.x + Math.cos(angle)*l, c.y + Math.sin(angle)*l);
		n = p.getNormal(c);
	}
	
	
	//*********************************************************
	// Check Collision
	//
	//  check for and report collisions with the ball
	//*********************************************************
	public CollisionData CheckCollision(Ball ball)
	{
		// the flipper is not moving, treat as a line
		if (rot == 0)
		{
			p.set(c.x + Math.cos(angle)*l, c.y + Math.sin(angle)*l);
			n = p.getNormal(c);
			CollisionData data = CheckCollisionLine(ball);
			
			if (data != null)
			{
				return data;
			}
			
			n = c.getNormal(p);
			data = CheckCollisionLine(ball);
			
			if (data != null)
			{
				return data;
			}
			
			data = CheckCollisionPoint(c, ball);
			if (data != null)
			{
				return data;
			}
			
			data = CheckCollisionPoint(p, ball);
			if (data != null)
			{
				return data;
			}
			
			return null;
		}
		
		// the flipper is moving
		else
		{
			if (rot > 0)
			{
				pUpper = new vector2d(c.x + Math.cos(angle)*l, c.y + Math.sin(angle)*l);
				pLower = new vector2d(c.x + Math.cos(angle + rot)*l, c.y + Math.sin(angle + rot)*l);
				
				nUpper = pUpper.getNormal(c);
				nLower = c.getNormal(pLower);
				
				pInitial = pUpper.copy();
				nInitial = pUpper.copy();
				
				pFinal = pLower.copy();
				nFinal = nLower.copy();
			}
			else
			{
				pUpper = new vector2d(c.x + Math.cos(angle + rot)*l, c.y + Math.sin(angle + rot)*l);
				pLower = new vector2d(c.x + Math.cos(angle)*l, c.y + Math.sin(angle)*l);
				
				nUpper = pUpper.getNormal(c);
				nLower = c.getNormal(pLower);
				
				pInitial = pLower.copy();
				nInitial = pLower.copy();
				
				pFinal = pUpper.copy();
				nFinal = nUpper.copy();
			}
			
			CollisionData data;
			
			// check if the ball is inside of the line's path
			data = CheckBallInside(ball);
			
			if (data != null)
			{
				System.out.println("inside");
				return data;
			}
			
			// check if the ball collides with the initial position of the line
			p.set(pInitial);
			n = nInitial.copy();
			
			data = CheckCollisionLine(ball);
			if (data != null)
			{
				System.out.println("inital");
				return data;
			}
			
			// check if the ball collides with the final position of the line
			p.set(pFinal);
			n = nFinal.copy();
			
			data = CheckCollisionLine(ball);
			if (data != null)
			{
				// calculate the amount of energy to transfer to the ball
				vector2d e = data.n.copy();
				e.scale(lineCoefficient * Math.abs(rot) * inertia);
				data.e = e;
				
				System.out.println("final");
				
				return data;
			}
			
			// check if the ball collides with the pivot point
			data = CheckCollisionPoint(c, ball);
			if (data != null)
			{
				System.out.println("pivot");
				return data;
			}
			
			// check if the ball collides with the side
			//data = CheckCollisionSide(ball);
			//if (data != null)
			//{
			//	System.out.println("side");
			//	return data;
			//}
			
			// check if the ball collides with the initial end point
			data = CheckCollisionPoint(pInitial, ball);
			if (data != null)
			{
				System.out.println("inital end");
				return data;
			}
			
			// check if the ball collides with the final point
			data = CheckCollisionPoint(pFinal, ball);
			if (data != null)
			{
				// calculate the amount of energy to transfer to the ball
				vector2d e = data.n.copy();
				e.scale(Math.abs(rot) * inertia);
				data.e = e;
				
				System.out.println("final end");
				return data;
			}
			
			return null;
		}
	}
	
	//*********************************************************
	// Check Ball Inside
	//
	//  check if the ball is inside the flipper's path
	//*********************************************************
	private CollisionData CheckBallInside(Ball ball)
	{
		vector2d c_ballPos = new vector2d(c, ball.pos);
		
		// check if the distance from the pivot point
		// if the distance is less than the ball's radius, ignore because that means the ball is inside the pivot point, which should not be possible
		if (c_ballPos.magnitude <= l + ball.r && c_ballPos.magnitude > ball.r)
		{
			// get the distance from the ball to the upper and lower lines
			double dUpper = c_ballPos.dot(nUpper);
			double dLower = c_ballPos.dot(nLower);
			

			// check if the ball is inside both the top and bottom lines
			if (dUpper < ball.r && dLower < ball.r)
			{
				// set the point of collision on the line by scaling c->pFinal normal by c->ballPos magnitude and adding it to c
				vector2d PoCL = new vector2d(c, pFinal);
				PoCL.scale(c_ballPos.magnitude / PoCL.magnitude); //(magnitude is now c_ballPos.magnitude)
				PoCL.add(c);
				
				// set the point of collision by projecting the ball along the normal of the final line ballR units
				vector2d PoC = PoCL.copy();
				vector2d nFinalR = nFinal.copy();
				
				// if the ball is hitting the end of the rotating line
				if (c_ballPos.magnitude > l)
				{
					// don't move the ball all the way out
					nFinalR.scale(Math.sqrt(ball.r*ball.r - (c_ballPos.magnitude - l)*(c_ballPos.magnitude - l)) + 0.1d);
					PoC.add(nFinalR);
					
					n = new vector2d(pFinal, PoC);
					n.normalize();
				}
				else
				{
					nFinalR.scale(ball.r + 0.2d);
					PoC.add(nFinalR);
					
					n.set(nFinal);
				}
				
				
				// calculate the amount of energy to transfer to the ball
				// calculate the Coefficient of the collision along line of the length of the line
				PoCL.subtract(c);
				lineCoefficient = PoCL.magnitude / l;
				
				if (lineCoefficient > 1.0d)
					lineCoefficient = 0.0d;
				
				vector2d e = n.copy();
				e.scale(lineCoefficient * Math.abs(rot) * inertia); 
				
				// check to see if the ball is moving in the same direction as the flipper
				double idp = ball.vel.dot(n);
				double aDamping;
				
				if (idp > 0)
				{
					// if so, set the damping to -1. This will keep the ball's velocity from being changed BEFORE the energy from the flipper is added (no damping will occur sense the ball is moving in the same direction as the flipper, therefore no loss in energy, only energy added)
					aDamping = -1.0d;
				}
				else
					aDamping = damping;
				
				return new CollisionData(PoC, n, 0.0001d, aDamping, friction, e, this);
			}
		}
		
		return null;
	}
	
	
	//*********************************************************
	// Check Collision Line
	//
	//  checks if the ball collides with a line specified by
	//   c, p, and n
	//*********************************************************
	private CollisionData CheckCollisionLine(Ball ball)
	{
		// get the inner dot product
		double idp = ball.vel.dot(n);
		
		// if it is positive, we are moving in the same direction as the normal
		if (idp > 0)
			return null;
		
		// calculate how long the point on the ball nearest the line takes to hit the line
		// the nearest point on the ball is simply the center of the ball minus the normal times the radius of the ball
		vector2d ballPosNear = new vector2d(ball.pos.x - n.x * ball.r, ball.pos.y - n.y * ball.r);
		
		// solve for when the nearest position collides with the line
		// p1->(ballPosNear) dot n = 0
		// ballPosNear = <Box + VxT i, Boy + VyT j>
		if (ball.vel.x * n.x + ball.vel.y * n.y == 0)
			return null;
		
		double t = ((c.x - ballPosNear.x) * n.x + (c.y - ballPosNear.y) * n.y)/(ball.vel.x * n.x + ball.vel.y * n.y);
		
		//double d1 = new vector2d(p1, ball.posLast).dot(n);
		//double d2 = new vector2d(p1, ball.pos).dot(n);
		//double t = (d1 - ball.r) / (d1 - d2);
		
		// make sure the collision will happen during this frame
		if (t < 0 || t > ball.time)
			return null;
		
		// calculate the point of collision on the line u
		// PosNear + vel*t = p1 + u(p2 - p1)
		// u = (PosNear + vel*t - p1)/(p2 - p1)
		double u;
		
		if (p.x == c.x)
			u = (ballPosNear.y + ball.vel.y*t - c.y)/(p.y - c.y);
		else
			u = (ballPosNear.x + ball.vel.x*t - c.x)/(p.x - c.x);
		
		// make sure the point of collision is on the line
		if (u < 0 || u > 1)
			return null;
		
		// calculate the point of collision
		vector2d PoC = new vector2d(ball.pos.x + ball.vel.x * t, ball.pos.y + ball.vel.y * t);
		
		lineCoefficient = u;
		return new CollisionData(PoC, n, t, damping, friction, this);
	}
	
	
	//*********************************************************
	// Check Collision Point
	//
	//  checks if the ball collides with a point
	//*********************************************************
	private CollisionData CheckCollisionPoint(vector2d c, Ball ball)
	{
		// solve for the possible times of collision
		// ||(B + VT) - C|| = Br
		// sqrt(((Bx + VxT) - Cx)^2 + ((By + VyT) - Cy)^2) = Br
		double f = Math.pow(2*(ball.pos.x*ball.vel.x + ball.pos.y*ball.vel.y - c.x*ball.vel.x - c.y*ball.vel.y), 2) - 4*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y)*(-2*ball.pos.x*c.x - 2*ball.pos.y*c.y + ball.pos.x*ball.pos.x + ball.pos.y*ball.pos.y + c.x*c.x + c.y*c.y - ball.r*ball.r);
		
		if (f < 0)
			return null;
		
		double a = Math.sqrt(f);
		double b = -2*ball.pos.x*ball.vel.x - 2*ball.pos.y*ball.vel.y + 2*c.x*ball.vel.x + 2*c.y*ball.vel.y;
		double d = 2*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y);
		
		double t;
		double t1 = (a + b) / d;
		double t2 = (-a + b) / d;
		
		// both possible times of collision must be in front of the ball, otherwise we are already inside the ball
		if (t1 < 0 || t2 < 0)
			return null;
		
		// choose the time that is soonest
		if (t1 < t2)
			t = t1;
		else
			t = t2;
		
		// make sure the collision will happen in this frame
		if (t > ball.time)
			return null;
		
		// calculate the point of collision
		vector2d dPos = ball.vel.copy();
		dPos.scale(t);
		vector2d PoC = ball.pos.copy();
		PoC.add(dPos);
		
		// calculate the normal
		// (C - PoC)/(||C - PoC||)
		vector2d n = new vector2d(PoC, c);
		n.normalize();
		
		return new CollisionData(PoC, n, t, damping, friction, this);
	}
	
	
	//*********************************************************
	// Check Collision Side
	//
	//  checks if the ball collides with the side
	//*********************************************************
	/*private CollisionData CheckCollisionSide(Ball ball)
	{
		// check if the ball collides with the side
		// solve for the possible times of collision
		// ||(B + VT) - C|| = Br + Cr
		// sqrt(((Bx + VxT) - Cx)^2 + ((By + VyT) - Cy)^2) = Br + Cr
		double f = Math.pow(2*(ball.pos.x*ball.vel.x + ball.pos.y*ball.vel.y - c.x*ball.vel.x - c.y*ball.vel.y), 2) - 4*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y)*(-2*ball.pos.x*c.x - 2*ball.pos.y*c.y + ball.pos.x*ball.pos.x + ball.pos.y*ball.pos.y + c.x*c.x + c.y*c.y - l*l - 2*l*ball.r - ball.r*ball.r);
		
		if (f < 0)
			return null;
		
		double a = Math.sqrt(f);
		double b = -2*ball.pos.x*ball.vel.x - 2*ball.pos.y*ball.vel.y + 2*c.x*ball.vel.x + 2*c.y*ball.vel.y;
		double d = 2*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y);
		
		double t;
		double t1 = (a + b) / d;
		double t2 = (-a + b) / d;
		
		// both possible times of collision must be in front of the ball, otherwise we are already inside the circle
		if (t1 < 0 || t2 < 0)
			return null;
		
		// choose the time that is soonest
		if (t1 < t2)
			t = t1;
		else
			t = t2;
		
		// make sure the collision will happen in this frame
		if (t > ball.time)
			return null;
		
		// calculate the point of collision
		vector2d PoC = new vector2d(ball.pos.x + ball.vel.x * t, ball.pos.y + ball.vel.y * t);
		
		// calculate the normal
		// (C - PoC)/(||C - PoC||)
		n = new vector2d(PoC, c);
		n.normalize();
		
		// get the distances from the ball to the upper and lower lines
		vector2d c_PoC = new vector2d(c, PoC);
		
		double dUpper = c_PoC.dot(nUpper);
		double dLower = c_PoC.dot(nLower);
		
		if (dUpper > 0 || dLower > 0)
			return null;
		
		// calculate the amount of energy to transfer to the ball
		vector2d e = n.copy();
		e.scale(Math.abs(rot) * inertia);

		return new CollisionData(PoC, n, t, damping, friction, e, this);
	}*/
}