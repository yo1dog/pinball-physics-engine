package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.Table;
import Default.vector2d;

public class Ramp extends Shape
{
	private vector2d p1, p2, e;
	
	public Ramp(int x, int y, int width, int height, int zHeight, Part owner)
	{
		this.p1 = new vector2d(x, y);
		this.p2 = new vector2d(x + width, y + height);
		
		e =  Table.grav.copy();
		e.scale(zHeight);
		
		setBoundaries(x, y, x + width, y + height);
		setOwner(owner);
	}

	public CollisionData CheckCollision(Ball ball)
	{
		if (ball.pos.x > p1.x && ball.pos.x < p2.x && ball.pos.y > p1.y && ball.pos.y < p2.y)
		{
			CollisionDetected();
			
			vector2d PoC = new vector2d(ball.pos.x + ball.vel.x, ball.pos.y + ball.vel.y);
			return new CollisionData(PoC, new vector2d(), 1.0d, 0.0d, 0.0d, e, this);
		}
		
		return null;
	}
}
