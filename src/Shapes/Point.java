package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.vector2d;

public class Point extends Shape
{
	private vector2d c;
	
	public Point(int x, int y, Part owner)
	{
		c = new vector2d(x, y);
		
		setBoundaries(x, x+1, y, y+1);
		setOwner(owner);
	}
	
	public CollisionData CheckCollision(Ball ball)
	{
		// solve for the possible times of collision
		// ||(B + VT) - C|| = Br
		// sqrt(((Bx + VxT) - Cx)^2 + ((By + VyT) - Cy)^2) = Br
		double f = Math.pow(2*(ball.pos.x*ball.vel.x + ball.pos.y*ball.vel.y - c.x*ball.vel.x - c.y*ball.vel.y), 2) - 4*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y)*(-2*ball.pos.x*c.x - 2*ball.pos.y*c.y + ball.pos.x*ball.pos.x + ball.pos.y*ball.pos.y + c.x*c.x + c.y*c.y - ball.r*ball.r);
		
		if (f < 0)
			return null;
		
		double a = Math.sqrt(f);
		double b = -2*ball.pos.x*ball.vel.x - 2*ball.pos.y*ball.vel.y + 2*c.x*ball.vel.x + 2*c.y*ball.vel.y;
		double d = 2*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y);
		
		double t;
		double t1 = (a + b) / d;
		double t2 = (-a + b) / d;
		
		// both possible times of collision must be in front of the ball, otherwise we are already inside the ball
		if (t1 < 0 || t2 < 0)
			return null;
		
		// choose the time that is soonest
		if (t1 < t2)
			t = t1;
		else
			t = t2;
		
		// make sure the collision will happen in this frame
		if (t > ball.time)
			return null;
		
		// calculate the point of collision
		vector2d dPos = ball.vel.copy();
		dPos.scale(t);
		vector2d PoC = ball.pos.copy();
		PoC.add(dPos);
		
		// calculate the normal
		// (C - PoC)/(||C - PoC||)
		vector2d n = new vector2d(PoC, c);
		n.normalize();
		
		CollisionDetected();
		return new CollisionData(PoC, n, t, damping, friction, this);
	}
}
