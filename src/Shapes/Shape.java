/*****************************************************************
 * Shape.java
 * 
 * The Shape class the parent class of all the shapes classes. It defines a primitive
 *  collision shape. When the ball collides with this shape it is notified via the
 *  CollisionDetected method in which it notifies its Part object parent.
 */

package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.Table;

public abstract class Shape
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	private Part owner;                   // Part object that owns this shape
	private int left, right, top, bottom; // borders (for simple collisions)
	
	public double damping = Table.defaultDamping;   // damping (bounciness)
	public double friction = Table.defaultFriction; // friction scalar
	
	
	//*********************************************************
	// Boundary Methods
	//
	//*********************************************************
	// set
	protected final void setBoundaries(int left, int right, int top, int bottom)
	{
		this.left = left;
		this.right = right;
		this.top = top;
		this.bottom = bottom;
	}
	// get
	public final int getLeft()
	{
		return left;
	}
	public final int getRight()
	{
		return right;
	}
	public final int getTop()
	{
		return top;
	}
	public final int getBottom()
	{
		return bottom;
	}
	
	//*********************************************************
	// Set Owner
	//
	//  sets the owner of this shape
	//*********************************************************
	protected final void setOwner(Part part)
	{
		owner = part;
	}
	
	//*********************************************************
	// Check Simple Collision
	//
	//  checks if the ball collides with the shape's bounding box
	//*********************************************************
	public final boolean CheckSimpleCollision(Ball ball)
	{
		return (ball._right >= left && ball._left <= right && ball._bottom >= top && ball._top <= bottom);
	}
	
	
	//*********************************************************
	// Collision Detected
	//
	//  Called when the ball collides with this shape
	//*********************************************************
	public final void CollisionDetected()
	{
		// pass the collision up to the shapes' part
		owner.CollisionDetected(this);
	}
	
	
	//*********************************************************
	// Check Collision
	//
	//  checks if the ball collides with the shape and returns a
	//   CollisionData structure that contains the position, angle,
	//   and damping of intersection.
	//*********************************************************
	public abstract CollisionData CheckCollision(Ball ball);
}
