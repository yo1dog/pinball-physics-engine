package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.vector2d;

public class Line extends Shape
{
	private vector2d p1, p2, n;
	
	public Line(int x1, int y1, int x2, int y2, Part owner)
	{
		this.p1 = new vector2d(x1, y1);
		this.p2 = new vector2d(x2, y2);
		n = p1.getNormal(p2);
		
		setBoundaries(Math.min(x1, x2), Math.max(x1, x2), Math.min(y1, y2), Math.max(y1, y2));
		setOwner(owner);
	}

	public CollisionData CheckCollision(Ball ball)
	{
		// get the inner dot product
		double idp = ball.vel.dot(n);
		
		// if it is positive, we are moving in the same direction as the normal
		if (idp > 0)
			return null;
		
		// calculate how long the point on the ball nearest the line takes to hit the line
		// the nearest point on the ball is simply the center of the ball minus the normal times the radius of the ball
		vector2d ballPosNear = new vector2d(ball.pos.x - n.x * ball.r, ball.pos.y - n.y * ball.r);
		
		// solve for when the nearest position collides with the line
		// p1->(ballPosNear) dot n = 0
		// ballPosNear = <Box + VxT i, Boy + VyT j>
		if (ball.vel.x * n.x + ball.vel.y * n.y == 0)
			return null;
		
		double t = ((p1.x - ballPosNear.x) * n.x + (p1.y - ballPosNear.y) * n.y)/(ball.vel.x * n.x + ball.vel.y * n.y);
		
		//double d1 = new vector2d(p1, ball.posLast).dot(n);
		//double d2 = new vector2d(p1, ball.pos).dot(n);
		//double t = (d1 - ball.r) / (d1 - d2);
		
		// make sure the collision will happen during this frame
		if (t < 0 || t > ball.time)
			return null;
		
		// calculate the point of collision on the line u
		// PosNear + vel*t = p1 + u(p2 - p1)
		// u = (PosNear + vel*t - p1)/(p2 - p1)
		double u;
		
		if (p2.x == p1.x)
			u = (ballPosNear.y + ball.vel.y*t - p1.y)/(p2.y - p1.y);
		else
			u = (ballPosNear.x + ball.vel.x*t - p1.x)/(p2.x - p1.x);
		
		// make sure the point of collision is on the line
		if (u < 0 || u > 1)
			return null;
		
		// calculate the point of collision
		vector2d dPos = ball.vel.copy();
		dPos.scale(t);
		vector2d PoC = ball.pos.copy();
		PoC.add(dPos);
		
		CollisionDetected();
		return new CollisionData(PoC, n, t, damping, friction, this);
	}
}
