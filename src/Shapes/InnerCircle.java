package Shapes;
import TableParts.Part;
import Default.Ball;
import Default.CollisionData;
import Default.vector2d;

public class InnerCircle extends Shape
{
	private vector2d c, angleN;
	private int r;
	
	public InnerCircle(int x, int y, int r, Part owner)
	{
		c = new vector2d(x, y);
		angleN = new vector2d(0, -1);
		this.r = r;
		
		setBoundaries(x - r, x + r, y - r, y + r);
		setOwner(owner);
	}

	public CollisionData CheckCollision(Ball ball)
	{
		/*// check if the ball is already outside the circle
		vector2d c_ballPos = new vector2d(c, ball.pos);
		if (c_ballPos.magnitude > r - ball.r)
		{
			vector2d PoC = c_ballPos.copy();
			PoC.scale((r - ball.r)/PoC.magnitude);
			PoC.add(c);
			
			// make sure the point of collision is within the angle
			vector2d c_PoC = new vector2d(c, PoC);
			
			if (c_PoC.dot(angleN) < 0)
				return null;
			
			// calculate the normal
			// (C - PoC)/(||C - PoC||)
			vector2d n = new vector2d(c, PoC);
			n.normalize();
			
			CollisionDetected();
			return new CollisionData(PoC, n, 0.0001d, damping, friction, this);
		}*/
		
		// solve for the possible times of collision
		// ||(B + VT) - C|| = Br + Cr
		// sqrt(((Bx + VxT) - Cx)^2 + ((By + VyT) - Cy)^2) = Br + Cr
		double f = Math.pow(2*(ball.pos.x*ball.vel.x + ball.pos.y*ball.vel.y - c.x*ball.vel.x - c.y*ball.vel.y), 2) - 4*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y)*(-2*ball.pos.x*c.x - 2*ball.pos.y*c.y + ball.pos.x*ball.pos.x + ball.pos.y*ball.pos.y + c.x*c.x + c.y*c.y - r*r + 2*r*ball.r + ball.r*ball.r);
		
		if (f < 0)
			return null;
		
		double a = Math.sqrt(f);
		double b = -2*ball.pos.x*ball.vel.x - 2*ball.pos.y*ball.vel.y + 2*c.x*ball.vel.x + 2*c.y*ball.vel.y;
		double d = 2*(ball.vel.x*ball.vel.x + ball.vel.y*ball.vel.y);
		
		double t;
		double t1 = (a + b) / d;
		double t2 = (-a + b) / d;
		
		// choose the time that is farthest
		if (t1 < t2)
			t = t2;
		else
			t = t1;
		
		// make sure the collision will happen in this frame
		if (t > ball.time)
			return null;
		
		// calculate the point of collision
		vector2d dPos = ball.vel.copy();
		dPos.scale(t);
		vector2d PoC = ball.pos.copy();
		PoC.add(dPos);
		
		// make sure the point of collision is within the angle
		vector2d c_PoC = new vector2d(c, PoC);
		
		if (c_PoC.dot(angleN) < 0)
			return null;
		
		// calculate the normal
		// (C - PoC)/(||C - PoC||)
		vector2d n = new vector2d(c, PoC);
		n.normalize();
		
		CollisionDetected();
		return new CollisionData(PoC, n, t, damping, friction, this);
	}
}
