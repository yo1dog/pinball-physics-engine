/*****************************************************************
 * Ball.java
 * 
 * The Ball class first detects simple collisions (bounding boxes) with the Shape
 *  classes that are contained in the Part classes. It then takes all of these Shapes
 *  and detects precise collisions and moves accordingly.
 */

package Default;
import Shapes.Shape;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Graphics;

public class Ball
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	public vector2d pos, startPos;      // position
	public vector2d vel;                // velocity
	private final double maxVel = 40.0d; // max velocity
	public double time = 1.0d;		    // time left in the frame
	public final int r = 10;            // radius
	
	public double _left, _right, _top, _bottom;  // borders (for calculating simple collisions)

	
	
	//*********************************************************
	// Initiate Method
	//
	//*********************************************************
	public Ball(int x, int y)
	{
		startPos = new vector2d(x, y);
		pos = startPos.copy();
		vel = new vector2d(0, 0);
		
		ReCalculateSimpleCollisionBorders();
	}
	
	
	//*********************************************************
	// Re-Calculate Simple Collision Borders
	//
	//  updates the simple collision borders
	//*********************************************************
	private void ReCalculateSimpleCollisionBorders()
	{
		_left   = pos.x - vel.magnitude - r - 2;
		_right  = pos.x + vel.magnitude + r + 2;
		_top    = pos.y - vel.magnitude - r - 2;
		_bottom = pos.y + vel.magnitude + r + 2;
	}
	
	
	//*********************************************************
	// Move
	//
	//*********************************************************
	public void Move()
	{
		// only check for collisions and move if the ball has a velocity
		if (vel.magnitude != 0)
		{
			// time remaining in this frame
			time = 1.0d;
			
			while(time > 0)
			{
				Shape[] shapes = getSimpleCollisionShapes();
				CollisionData[] posibleDataSet = getPossibleCollisions(shapes);
				CollisionData[] dataSet = getCollisions(posibleDataSet);
				
				if (dataSet == null)
					break;
				
				if (dataSet.length == 0)
					break;
				
				// add up the normals of all concurrent collisions?
				CollisionData data = dataSet[0];
				
				// move the ball to the point of collision
				pos.set(data.PoC);
				
				// calculate the new velocity after the collision
				// apply damping
				// vel -= ((1 + damping) * vel dot n) * n
				vector2d dampedNormal = data.n.copy();
				dampedNormal.scale(vel.dot(data.n) * (1 + data.damping));
				vel.subtract(dampedNormal);
				
				// apply friction
				// vel -= (1 + damping) * vel dont t) * t
				vector2d t = data.n.getPerpendicular();
				t.scale(vel.dot(t) * (data.friction));
				vel.subtract(t);
				
				// apply added energy
				vel.add(data.e);
				
				// cap the velocity at the max speed
				if (vel.magnitude > maxVel)
					vel.scale(maxVel / vel.magnitude);
				
				// subtract the time from the total time
				time -= data.time;
				
				// recalculate now that the position and velocity have changed
				ReCalculateSimpleCollisionBorders();
			}
		
			// no more collisions, move the ball for the remaining time
			vector2d dPos = vel.copy();
			dPos.scale(time);
			
			pos.add(dPos);
			
			if (pos.y > Main.height + r)
				pos.set(startPos);
		}
		
		// apply gravity
		vel.add(Table.grav);
		
		// apply table friction
		vel.scale(1 - Table.tableFriction);
		
		// recalculate now that the position and velocity have changed
		ReCalculateSimpleCollisionBorders();
	}
	
	//*********************************************************
	// Get Simple Collision Shapes
	//
	//  returns all the shapes who's bounding box collides with
	//   the ball
	//*********************************************************
	private Shape[] getSimpleCollisionShapes()
	{
		ArrayList<Shape> collidedShapes = new ArrayList<Shape>();
		Shape[] shapes = Table.getShapes();
		
		for (int i = 0; i < shapes.length; i++)
		{
			if (shapes[i].CheckSimpleCollision(this))
				collidedShapes.add(shapes[i]);
		}
		
		Shape[] rtrn = new Shape[collidedShapes.size()];
		collidedShapes.toArray(rtrn);
		collidedShapes.clear();
		collidedShapes = null;
		
		return rtrn;
	}
	
	
	//*********************************************************
	// Get Possible Collisions
	//
	//  returns a list of collision data for all collisions with
	//   the provided set of shapes
	//*********************************************************
	private CollisionData[] getPossibleCollisions(Shape[] shapes)
	{
		ArrayList<CollisionData> collidedShapes = new ArrayList<CollisionData>();
		
		for (int i = 0; i < shapes.length; i++)
		{
			CollisionData data = shapes[i].CheckCollision(this);
			
			if (data != null)
				collidedShapes.add(data);
		}
		
		CollisionData[] rtrn = new CollisionData[collidedShapes.size()];
		collidedShapes.toArray(rtrn);
		collidedShapes.clear();
		collidedShapes = null;
		
		return rtrn;
	}
	
	
	//*********************************************************
	// Get Collisions
	//
	//  returns the collision of the ball based on the given
	//   possible collision data
	//
	//  The collision is the closest of all the possible
	//*********************************************************
	private CollisionData[] getCollisions(CollisionData[] data)
	{
		// if the data set is empty, return
		if (data.length == 0)
			return null;
		
		// start the shortest time at an impossibly high value
		double shortestTime = 2;
		ArrayList<CollisionData> collisions = new ArrayList<CollisionData>();
		
		for (int i = 0; i < data.length; i++)
		{
			if (data[i].time < shortestTime)
			{
				collisions.clear();
				collisions.add(data[i]);
				shortestTime = data[i].time;
			}
			else if (data[i].time == shortestTime)
				collisions.add(data[i]);
		}
		
		CollisionData[] rtrn = new CollisionData[collisions.size()];
		collisions.toArray(rtrn);
		collisions.clear();
		collisions = null;
		
		return rtrn;
	}
	
	
	//*********************************************************
	// Mouse
	//
	//*********************************************************
	public void MouseDown()
	{
		pos.x = Main.mouseX;
		pos.y = Main.mouseY;
		vel.set(0, 0);
		
		ReCalculateSimpleCollisionBorders();
	}
	
	//*********************************************************
	// Keyboard
	//
	//*********************************************************
	public void KeyDown(int key)
	{
		if (!Main.debug)
		{
			if (key == 119)
			{
				vel.y -= 5;
				ReCalculateSimpleCollisionBorders();
			}
		}
	}
	
	//*********************************************************
	// Draw
	//
	//*********************************************************
	public void Draw(Graphics g)
	{
		g.setColor(Color.lightGray);
		g.drawOval((int)pos.x - r, (int)pos.y - r, r*2, r*2);
		
		//g.setColor(Color.white);
		//g.drawRect((int)_left, (int)_top, (int)(_right - _left), (int)(_bottom - _top));
		
		/*g.setColor(Color.lightGray);
		g.drawOval((int)_pos1.x - r, (int)_pos1.y - r, r*2, r*2);
		g.drawOval((int)_pos2.x - r, (int)_pos2.y - r, r*2, r*2);
		g.drawLine((int)_pos1.x, (int)_pos1.y, (int)_pos2.x, (int)_pos2.y);
		
		
		if (debugCollisionData != null)
		{
			// b * ( -2*(V dot N)*N + V )
			vector2d rVel = vel.copy();
			rVel.subtract(debugCollisionData.r);
			
			vector2d vNew = debugCollisionData.n.copy();
			vNew.scale(-2 * rVel.dot(debugCollisionData.n));
			vNew.add(rVel);
			vNew.scale(debugCollisionData.damping);
			
			g.setColor(Color.green);
			g.drawLine((int)(debugCollisionData.pos.x - debugCollisionData.n.x * r), (int)(debugCollisionData.pos.y - debugCollisionData.n.y * r), (int)(debugCollisionData.pos.x + debugCollisionData.n.x * (20 + r)), (int)(debugCollisionData.pos.y + debugCollisionData.n.y * (20 + r)));
			g.drawLine((int)(debugCollisionData.pos.x), (int)(debugCollisionData.pos.y), (int)(debugCollisionData.pos.x + vNew.x), (int)(debugCollisionData.pos.y + vNew.y));
			
			g.setColor(Color.lightGray);
			g.drawOval((int)debugCollisionData.pos.x - r, (int)debugCollisionData.pos.y - r, r*2, r*2);
		}
		
		if (Main.debug)
		{
			vel.x = Main.mouseX - pos.x;
			vel.y = Main.mouseY - pos.y;
			ReCalculateCollisionData();
			
			Shape[] shapes = getSimpleCollisionShapes();
			vector2d oPos = pos.copy();
			
			g.setColor(Color.red);
			g.drawLine((int)_pos1.x, (int)_pos1.y, (int)_pos2.x, (int)_pos2.y);
			
			for(int i = 0; i < 10; i++)
			{
				g.setColor(Color.lightGray);
				g.drawOval((int)_pos1.x - r, (int)_pos1.y - r, r*2, r*2);
				
				CollisionData[] dataSet = getCollisions(shapes);
				CollisionData data = getCollision(dataSet);
				
				if (data == null)
				{
					g.drawOval((int)_pos2.x - r, (int)_pos2.y - r, r*2, r*2);
					g.drawLine((int)_pos1.x, (int)_pos1.y, (int)_pos2.x, (int)_pos2.y);
					break;
				}
				
				vel.subtract(data.r);
				
				vector2d n = data.n.copy();
				vector2d tan = data.n.getPerpendicular();
				
				n.scale(-2 * vel.dot(n) * data.damping);
				tan.scale(-1 * vel.dot(tan) * data.friction);
				
				if (tan.getMagnitude() < Table.staticFriction)
					tan.scale(10);
				
				n.add(tan);
				n.add(vel);
				
				vel.set(n);
				pos.set(data.pos);
				lastCollidedShape = data.shape;
				
				g.drawLine((int)_pos1.x, (int)_pos1.y, (int)pos.x, (int)pos.y);
				
				g.setColor(Color.green);
				g.drawLine((int)(data.pos.x - data.n.x * r), (int)(data.pos.y - data.n.y * r), (int)(data.pos.x + data.n.x * (20 + r)), (int)(data.pos.y + data.n.y * (20 + r)));
				
				ReCalculateCollisionData();
			}
			
			lastCollidedShape = null;
			pos.set(oPos);
		}*/
	}
}
