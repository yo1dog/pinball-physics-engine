package Default;
import Shapes.Shape;

public final class CollisionData
{
	public vector2d PoC, n, e;
	public double time, damping, friction;
	public Shape shape;
	
	public CollisionData(vector2d PoC, vector2d n, double time, double damping, double friction, Shape shape)
	{
		this.PoC = PoC;
		this.n = n;
		this.time = time;
		this.damping = damping;
		this.friction = friction;
		e = new vector2d(0.0d, 0.0d);
		this.shape = shape;
	}
	public CollisionData(vector2d PoC, vector2d n, double time, double damping, double friction, vector2d e, Shape shape)
	{
		this.PoC = PoC;
		this.n = n;
		this.time = time;
		this.damping = damping;
		this.friction = friction;
		this.e = e;
		this.shape = shape;
	}
}
