package Default;
import java.awt.*;
import java.applet.Applet;

import TableParts.*;

public class Main extends Applet implements Runnable
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	
	private static final long serialVersionUID = 8112706461677260009L;
	public static final int  width =  601; // width of the room
	public static final int  height = 901; // height of the room
	private final int speed = 12;          // ms between frames
	
	private Ball[] balls = new Ball[1];
	private FlipperLeft flipperLeft = new FlipperLeft(150, 800);
	private FlipperRight flipperRight = new FlipperRight(350, 800);
	
	public static int mouseX, mouseY;
	
	private Thread th;     // thread
	private Image dbImage; // game image
	private Graphics dbg;  // graphics object
	public static boolean debug = false;

	
	
	//*********************************************************
	// Initiate Method
	//
	//*********************************************************
	public void init()
	{
		balls[0] = new Ball(575, 750);
		//balls[1] = new Ball(400, 50);
		setBackground(Color.black);
		this.setSize(width, height);
		
		Table.AddPart(new Wall(0, 0, 500, 0, false));
		Table.AddPart(new Wall(50, 700, 0, 0, false));
		Table.AddPart(new Wall(500, 200, 450, 700, false));
		
		Table.AddPart(new Wall(500, 0, 550, 100, false));
		Table.AddPart(new Wall(550, 100, 600, 300, false));
		Table.AddPart(new Wall(550, 100, 500, 200, false));
		Table.AddPart(new Wall(600, 300, 600, 800, false));
		Table.AddPart(new Wall(550, 800, 500, 200, false));
		Table.AddPart(new Wall(600, 800, 550, 800, false));
		
		Table.AddPart(new Wall(150, 800, 50, 700, false));
		Table.AddPart(new Wall(450, 700, 350, 800, false));

		Table.AddPart(new Wall(280, 80, 400, 200, true));
		Table.AddPart(new Wall(400, 200, 250, 350, true));
		Table.AddPart(new Wall(250, 350, 100, 200, true));
		Table.AddPart(new Wall(100, 200, 220, 80, true));
		Table.AddPart(new Bumper(210, 160, 15));
		Table.AddPart(new Bumper(290, 160, 15));
		Table.AddPart(new Bumper(290, 240, 15));
		Table.AddPart(new Bumper(210, 240, 15));
		Table.AddPart(new Bumper(370, 200, 15));
		Table.AddPart(new Bumper(130, 200, 15));
		Table.AddPart(new Bumper(250, 320, 15));
		
		//Table.AddPart(new Bumper(125, 355, 15));
		Table.AddPart(new RampTest(125, 355, 100, 200, 3));
		Table.AddPart(new Wall(125, 355, 125, 555, true));
		Table.AddPart(new Wall(225, 355, 225, 555, true));
		
		Table.AddPart(new GaurdLeft(120, 570));
		Table.AddPart(new GaurdRight(380, 570));
		
		Table.AddPart(flipperLeft);
		Table.AddPart(flipperRight);
	}
	
	
	//*********************************************************
	// Thread Methods
	//
	//*********************************************************
	// start thread
	public void start()
	{
		th = new Thread(this);
		th.start();
	}
	
	// stop thread
	@SuppressWarnings("deprecation")
	public void stop()
	{
		Table.Clear();
		th.stop();
	}
	
	
	//*********************************************************
	// Mouse Methods
	//
	//*********************************************************
	// mouse is pressed
	public boolean mouseDown(Event e,int x,int y)
	{
		balls[0].MouseDown();
		return true;
	}
	
	// mouse is released
	public boolean mouseUp(Event e,int x,int y)
	{
		return true;
	}
	
	// mouse moves
	public boolean mouseMove(Event e, int x, int y)
	{
		mouseX = x;
		mouseY = y;
		
		return true;
	}
	
	
	//*********************************************************
	// Keyboard Methods
	//
	//*********************************************************
	// key is pressed
	public boolean keyDown(Event e, int key)
	{
		if (key == 109)
			debug = !debug;
		else if (key == 97) // a
		{
			flipperLeft.Activate();
		}
		else  if (key == 100) // d
		{
			flipperRight.Activate();
		}
		else if (key == 32) // space
		{
			balls[0].vel.y -= 20;
		}
		
		balls[0].KeyDown(key);
		return true;
	}
	
	// key is release
	public boolean keyUp(Event e, int key)
	{
		if (key == 97) // a
		{
			flipperLeft.Deactivate();
		}
		else  if (key == 100) // d
		{
			flipperRight.Deactivate();
		}
		
		balls[0].KeyDown(key);
		return true;
	}
	
	
	
	//*********************************************************
	// Main Loop Method
	//
	//*********************************************************
	public void run()
	{
		// set thread priority
		th.setPriority(Thread.MAX_PRIORITY);
		
		// start Loop
		while (true)
		{
			if (!debug)
			{
				flipperLeft.Rotate();
				flipperRight.Rotate();
				
				for (int i = 0; i < balls.length; i++)
					balls[i].Move();
			}
			
			flipperLeft.PostCheck();
			flipperRight.PostCheck();
				
			
			//------------------------------------------------
			// Post Operations
			//------------------------------------------------
			
			// re-draw the scene
			repaint();
			
			// operate the thread
			try
			{
				Thread.sleep(speed);
			}
			catch (InterruptedException ex)
			{
			}
			
			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
		}
	}
	
	
	//*********************************************************
	// Draw Game
	//
	//*********************************************************
	public void paint (Graphics g)
	{
		// create graphics object and rendering properties
		Graphics2D g2D = (Graphics2D)g;
		RenderingHints rh2 = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2D.setRenderingHints(rh2);

		Part[] parts = Table.getParts();
		for (int i = 0; i < parts.length; i++)
			parts[i].Draw(g);
		
		for (int i = 0; i < balls.length; i++)
			balls[i].Draw(g);
	}
	
	
	//*********************************************************
	// Update Screen
	//
	//*********************************************************
	public void update(Graphics g)
	{
		if (dbImage == null)
		{
			dbImage = createImage(width, height);
			dbg = dbImage.getGraphics();
		}
		
		dbg.setColor(Color.black);
		dbg.fillRect(0, 0, width, height);
		
		dbg.setColor(getForeground());
		paint(dbg);
		
		g.drawImage(dbImage, 0, 0, this);
	}
}