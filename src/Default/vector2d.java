package Default;

public final class vector2d
{
	public double x, y, magnitude;
	
	public vector2d()
	{
		x = 0.0d;
		y = 0.0d;
		magnitude = 0.0d;
	}
	public vector2d(double x, double y)
	{
		this.x = x;
		this.y = y;
		calcMagnitude();
	}
	
	public vector2d(vector2d v1, vector2d v2)
	{
		x = v2.x - v1.x;
		y = v2.y - v1.y;
		calcMagnitude();
	}
	
	public void add(vector2d vector)
	{
		x += vector.x;
		y += vector.y;
		calcMagnitude();
	}
	public void subtract(vector2d vector)
	{
		x -= vector.x;
		y -= vector.y;
		calcMagnitude();
	}
	
	public double dot(vector2d vector)
	{
		return x*vector.x + y*vector.y;
	}
	public void scale(double val)
	{
		x *= val;
		y *= val;
		magnitude *= val;
	}
	public vector2d getNormal(vector2d vector)
	{
		if (equals(vector))
			return null;
		vector2d normal = new vector2d(y - vector.y, vector.x - x);
		normal.normalize();
		
		return normal;
	}
	public void normalize()
	{
		scale(1 / magnitude);
		magnitude = 1.0d;
	}
	public vector2d getPerpendicular()
	{
		return new vector2d(y, -x);
	}
	
	public void set(vector2d vector)
	{
		x = vector.x;
		y = vector.y;
		calcMagnitude();
	}
	public void set(double x, double y)
	{
		this.x = x;
		this.y = y;
		calcMagnitude();
	}
	public vector2d copy()
	{
		return new vector2d(x, y);
	}
	
	public boolean equals(vector2d vector)
	{
		return (x == vector.x && y == vector.y);
	}
	
	private void calcMagnitude()
	{
		magnitude = Math.sqrt(x*x + y*y);
	}
}
