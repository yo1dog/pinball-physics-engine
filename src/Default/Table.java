/*****************************************************************
 * Table.java
 * 
 * The Table class contains some of the physics world constants as well as all
 *  the shapes that make up the board. 
 */

package Default;
import java.util.ArrayList;
import Shapes.Shape;
import TableParts.Part;

public final class Table
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	public static final vector2d grav = new vector2d(0, 0.1d); // gravity or downward force
	public static final double defaultDamping = 0.4d;          // default damping (bounciness) of collisions
	public static final double defaultFriction = 0.005d;        // default friction (0 - 1)
	public static final double tableFriction = 0.0d;            // table friction (0 - 1)
	
	private static ArrayList<Shape> shapes = new ArrayList<Shape>(); // shapes that make up the board
	private static ArrayList<Part> parts = new ArrayList<Part>();    // parts that make up the board
	
	
	//*********************************************************
	// Shape Methods
	//
	//*********************************************************
	// add
	public static void AddShape(Shape shape)
	{
		shapes.add(shape);
	}
	// get
	public static Shape[] getShapes()
	{
		Shape[] rtrn = new Shape[shapes.size()];
		shapes.toArray(rtrn);
		return rtrn;
	}
	
	
	//*********************************************************
	// Part Methods
	//
	//*********************************************************
	// add
	public static void AddPart(Part part)
	{
		parts.add(part);
	}
	// get
	public static Part[] getParts()
	{
		Part[] rtrn = new Part[parts.size()];
		parts.toArray(rtrn);
		return rtrn;
	}
	
	//*********************************************************
	// Clear
	//
	//*********************************************************
	public static void Clear()
	{
		shapes.clear();
		parts.clear();
		shapes = null;
		parts = null;
	}
}
