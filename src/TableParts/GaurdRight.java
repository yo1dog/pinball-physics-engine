package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.Line;
import Shapes.Point;

public class GaurdRight extends Part
{
	private int x1, y1, x2, y2, x3, y3;
	
	public GaurdRight(int x, int y)
	{
		this.x1 = x;
		this.y1 = y;
		x2 = x1 - 50;
		y2 = y1 + 150;
		x3 = x1 + 20;
		y3 = y1 + 100;
		
		Line l = new Line(x1, y1, x2, y2, this);
		l.damping = 1.5d;
		Table.AddShape(l);
		Table.AddShape(new Line(x2, y2, x3, y3, this));
		Table.AddShape(new Line(x3, y3, x1, y1, this));
		Table.AddShape(new Point(x1, y1, this));
		Table.AddShape(new Point(x2, y2, this));
		Table.AddShape(new Point(x3, y3, this));
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.red);
		
		g.drawLine(x1, y1, x2, y2);
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.blue);
		
		g.drawLine(x2, y2, x3, y3);
		g.drawLine(x3, y3, x1, y1);
	}
}
