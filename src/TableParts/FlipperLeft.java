package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.RotatingLine;

public class FlipperLeft extends Part
{
	//*********************************************************
	// Create Variables and Constants
	//
	//*********************************************************
	private int x, y;                                          // point the flipper rotates around
	private double x2, y2;                                      // endpoint of the flipper
	private double initialAngle = Math.PI * 0.18d;  // starting (rest) angle for the flipper
	private double flipAmount =   Math.PI * 0.4d; // amount the flipper flips
	private double flipSpeed =    Math.PI * -0.1d;  // speed at which the flipper flips
	private int l =              90;                          // length of the flipper
	
	private double t;   // angle of the flipper
	private double dt;  // angle amount the flipper has flipped
	private double afs; // absolute value of the flipSpeed
	
	private boolean flip;     // if the flipper is active
	private boolean atTop;    // if the flipper is at the top-most position
	private boolean atBottom; // if the flipper is at the bottom-most position
	
	private RotatingLine line;
	
	
	//*********************************************************
	// Initiate Methods
	//
	//*********************************************************
	public FlipperLeft(int x, int y)
	{
		this.x = x;
		this.y = y;
		Init();
	}
	public FlipperLeft(int x, int y, int l, double initialAngle, double flipAmount, double flipSpeed)
	{
		this.x = x;
		this.y = y;
		this.l = l;
		this.initialAngle = initialAngle;
		this.flipAmount = flipAmount;
		this.flipSpeed = flipSpeed;
		Init();
	}
	private void Init()
	{
		t = initialAngle;
		dt = 0;
		afs = Math.abs(flipSpeed);
		x2 = x + Math.cos(t) * l;
		y2 = y + Math.sin(t) * l;
		
		line = new RotatingLine(x, y, x2, y2, this);
		Table.AddShape(line);
	}
	
	
	//*********************************************************
	// Activate Methods
	//
	//  sets if the flipper should be active (flipping up) or not 
	//*********************************************************
	public void Activate()
	{
		flip = true;
	}
	public void Deactivate()
	{
		flip = false;
	}
	
	//*********************************************************
	// Rotate
	//
	//  rotates the line based on if it is active and its current
	//   position
	//*********************************************************
	public void Rotate()
	{
		// should flip up
		if (flip)
		{
			// is not already at the top
			if (!atTop)
			{
				// the flipper is moving up so it can no longer be at the bottom
				atBottom = false;
				
				// add to the angle
				dt += afs;
				t += flipSpeed;
				
				// set the new end points
				x2 = x + Math.cos(t) * l;
				y2 = y + Math.sin(t) * l;
				
				// check if the flipper is at the top
				if (dt >= flipAmount)
					atTop = true;
				
				// rotate the line
				line.Rotate(flipSpeed);
			}
		}
		else
		{
			// is not already at the bottom
			if (!atBottom)
			{
				// the flipper is moving down so it can no longer be at the top
				atTop = false;
				
				// add to the angle
				dt -= afs;
				t -= flipSpeed;
				
				// set the new end points
				x2 = x + Math.cos(t) * l;
				y2 = y + Math.sin(t) * l;
				
				// check if the flipper is at the bottom
				if (dt <= 0)
					atBottom = true;
				
				// rotate the line
				line.Rotate(-flipSpeed);
			}
		}
	}
	
	public void PostCheck()
	{
		line.PostCheck();
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.blue);
		
		g.drawLine(x, y, (int)x2, (int)y2);
	}
}