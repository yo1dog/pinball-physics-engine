/*****************************************************************
 * Part.java
 * 
 * The Part class is the parent class of all the part classes. It adds a set
 *  of Shape objects and is responsible for drawing them. If the ball collides
 *  with one of it's shapes, it is notified via the CollisionDetected method.
 */

package TableParts;
import java.awt.Graphics;
import Shapes.Shape;

public class Part
{
	protected int collided = 0;
	//*********************************************************
	// Draw
	//
	//*********************************************************
	public  void Draw(Graphics g)
	{
	}
	
	
	//*********************************************************
	// Collision Detected
	//
	//  Called when the ball collides with one of this part's
	//   shapes
	//*********************************************************
	public void CollisionDetected(Shape shape)
	{
		collided = 4;
	}
	
	public void StepCollisionCounter()
	{
		if (collided > 0)
			collided --;
	}
}
