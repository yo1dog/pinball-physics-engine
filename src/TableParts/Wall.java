package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.Line;
import Shapes.Point;

public class Wall extends Part
{
	private int x1, y1, x2, y2;
	
	public Wall(int x1, int y1, int x2, int y2, boolean _2d)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		
		Table.AddShape(new Line(x1, y1, x2, y2, this));
		if (_2d)
			Table.AddShape(new Line(x2, y2, x1, y1, this));
		Table.AddShape(new Point(x1, y1, this));
		Table.AddShape(new Point(x2, y2, this));
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.blue);
		
		g.drawLine(x1, y1, x2, y2);
		g.drawRect(x1 - 1, y1 - 1, 3, 3);
		g.drawRect(x2 - 1, y2 - 1, 3, 3);
	}
}
