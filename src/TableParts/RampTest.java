package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.Ramp;

public class RampTest extends Part
{
	private int x1, y1, x2, y2;
	
	public RampTest(int x, int y, int width, int height, int zHeight)
	{
		x1 = x;
		y1 = y;
		x2 = x1 + width;
		y2 = y1 + height;
		
		Table.AddShape(new Ramp(x, y, width, height, zHeight, this));
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.orange);
		
		g.drawLine(x1, y1, x1, y2);
		g.drawLine(x2, y1, x2, y2);
	}
}
