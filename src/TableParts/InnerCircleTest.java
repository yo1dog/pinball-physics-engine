package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.InnerCircle;

public class InnerCircleTest extends Part
{
	private int x, y, r;
	
	public InnerCircleTest(int x, int y, int r)
	{
		this.x = x;
		this.y = y;
		this.r = r;
		
		InnerCircle c = new InnerCircle(x, y, r, this);
		//c.damping = 1.0d;
		Table.AddShape(c);
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.gray);
		
		g.drawArc(x - r, y - r, r*2, r*2, 0, 180);
	}
}
