package TableParts;
import java.awt.Color;
import java.awt.Graphics;
import Default.Table;
import Shapes.Circle;

public class Bumper extends Part
{
	private int x, y, r;
	
	public Bumper(int x, int y, int r)
	{
		this.x = x;
		this.y = y;
		this.r = r;
		
		Circle c = new Circle(x, y, r, this);
		c.damping = 1.3d;
		Table.AddShape(c);
	}
	
	public void Draw(Graphics g)
	{
		StepCollisionCounter();
		
		if (collided > 0)
			g.setColor(Color.green);
		else
			g.setColor(Color.red);
		
		g.drawOval(x - r, y - r, r*2, r*2);
	}
}
