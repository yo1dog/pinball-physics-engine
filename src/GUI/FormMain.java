package GUI;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;


public class FormMain extends JPanel
{
   JButton m_jbutton1 = new JButton();
   JSlider m_jslider1 = new JSlider();
   JSlider m_jslider2 = new JSlider();
   JSlider m_jslider3 = new JSlider();
   JSlider m_jslider4 = new JSlider();
   JSlider m_jslider5 = new JSlider();
   JButton m_jbutton2 = new JButton();
   JTextArea m_jtextarea1 = new JTextArea();

   /**
    * Default constructor
    */
   public FormMain()
   {
      initializePanel();
   }

   /**
    * Main method for panel
    */
   public static void main(String[] args)
   {
      JFrame frame = new JFrame();
      frame.setSize(600, 400);
      frame.setLocation(100, 100);
      frame.getContentPane().add(new FormMain());
      frame.setVisible(true);

      frame.addWindowListener( new WindowAdapter()
      {
         public void windowClosing( WindowEvent evt )
         {
            System.exit(0);
         }
      });
   }

   /**
    * Adds fill components to empty cells in the first row and first column of the grid.
    * This ensures that the grid spacing will be the same as shown in the designer.
    * @param cols an array of column indices in the first row where fill components should be added.
    * @param rows an array of row indices in the first column where fill components should be added.
    */
   void addFillComponents( Container panel, int[] cols, int[] rows )
   {
      Dimension filler = new Dimension(10,10);

      boolean filled_cell_11 = false;
      CellConstraints cc = new CellConstraints();
      if ( cols.length > 0 && rows.length > 0 )
      {
         if ( cols[0] == 1 && rows[0] == 1 )
         {
            /** add a rigid area  */
            panel.add( Box.createRigidArea( filler ), cc.xy(1,1) );
            filled_cell_11 = true;
         }
      }

      for( int index = 0; index < cols.length; index++ )
      {
         if ( cols[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(cols[index],1) );
      }

      for( int index = 0; index < rows.length; index++ )
      {
         if ( rows[index] == 1 && filled_cell_11 )
         {
            continue;
         }
         panel.add( Box.createRigidArea( filler ), cc.xy(1,rows[index]) );
      }

   }

   /**
    * Helper method to load an image file from the CLASSPATH
    * @param imageName the package and name of the file to load relative to the CLASSPATH
    * @return an ImageIcon instance with the specified image file
    * @throws IllegalArgumentException if the image resource cannot be loaded.
    */
   public ImageIcon loadImage( String imageName )
   {
      try
      {
         ClassLoader classloader = getClass().getClassLoader();
         java.net.URL url = classloader.getResource( imageName );
         if ( url != null )
         {
            ImageIcon icon = new ImageIcon( url );
            return icon;
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      throw new IllegalArgumentException( "Unable to load image: " + imageName );
   }

   public JPanel createPanel()
   {
      JPanel jpanel1 = new JPanel();
      FormLayout formlayout1 = new FormLayout("FILL:77PX:NONE,FILL:170PX:NONE,FILL:84PX:NONE,RIGHT:170PX:NONE","FILL:901PX:NONE,CENTER:DEFAULT:NONE,CENTER:DEFAULT:NONE,CENTER:24PX:NONE,CENTER:79PX:NONE");
      CellConstraints cc = new CellConstraints();
      jpanel1.setLayout(formlayout1);

      m_jbutton1.setActionCommand("JButton");
      m_jbutton1.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jbutton1.setText("JButton");
      jpanel1.add(m_jbutton1,new CellConstraints(1,1,4,1,CellConstraints.FILL,CellConstraints.FILL));

      m_jslider1.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jslider1.setMajorTickSpacing(10);
      m_jslider1.setMaximum(1000);
      m_jslider1.setToolTipText("Friction put on the ball by the walls.");
      m_jslider1.setValue(5);
      jpanel1.add(m_jslider1,cc.xy(2,3));

      JLabel jlabel1 = new JLabel();
      jlabel1.setFont(new Font("SansSerif",Font.PLAIN,12));
      jlabel1.setText("Table Friction:");
      jlabel1.setToolTipText("Friction put on the ball by the table.");
      jpanel1.add(jlabel1,cc.xy(3,3));

      JLabel jlabel2 = new JLabel();
      jlabel2.setFont(new Font("SansSerif",Font.PLAIN,12));
      jlabel2.setText("Friction:");
      jlabel2.setToolTipText("Friction put on the ball by the walls.");
      jpanel1.add(jlabel2,cc.xy(1,3));

      m_jslider2.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jslider2.setMajorTickSpacing(10);
      m_jslider2.setMaximum(1000);
      m_jslider2.setToolTipText("Friction put on the ball by the table.");
      m_jslider2.setValue(0);
      jpanel1.add(m_jslider2,cc.xy(4,3));

      m_jslider3.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jslider3.setMajorTickSpacing(10);
      m_jslider3.setMaximum(200);
      m_jslider3.setToolTipText("The bounciness of the walls.");
      m_jslider3.setValue(75);
      jpanel1.add(m_jslider3,cc.xy(4,2));

      JLabel jlabel3 = new JLabel();
      jlabel3.setFont(new Font("SansSerif",Font.PLAIN,12));
      jlabel3.setText("Damping:");
      jlabel3.setToolTipText("The bounciness of the walls.");
      jpanel1.add(jlabel3,cc.xy(3,2));

      JLabel jlabel4 = new JLabel();
      jlabel4.setFont(new Font("SansSerif",Font.PLAIN,12));
      jlabel4.setText("Gravity:");
      jlabel4.setToolTipText("Downward force excerted on the ball.");
      jpanel1.add(jlabel4,cc.xy(1,2));

      m_jslider4.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jslider4.setMajorTickSpacing(10);
      m_jslider4.setToolTipText("Downward force excerted on the ball.");
      m_jslider4.setValue(10);
      jpanel1.add(m_jslider4,cc.xy(2,2));

      JLabel jlabel5 = new JLabel();
      jlabel5.setFont(new Font("SansSerif",Font.PLAIN,12));
      jlabel5.setText("Max Velocity:");
      jlabel5.setToolTipText("The maximum speed the ball can travel.");
      jpanel1.add(jlabel5,cc.xy(1,4));

      m_jslider5.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jslider5.setMajorTickSpacing(10);
      m_jslider5.setMaximum(200);
      m_jslider5.setMinimum(10);
      m_jslider5.setToolTipText("The maximum speed the ball can travel.");
      m_jslider5.setValue(40);
      jpanel1.add(m_jslider5,cc.xy(2,4));

      m_jbutton2.setActionCommand("Reset");
      m_jbutton2.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jbutton2.setText("Reset");
      jpanel1.add(m_jbutton2,cc.xy(4,4));

      m_jtextarea1.setAutoscrolls(true);
      m_jtextarea1.setBackground(new Color(255,255,255));
      m_jtextarea1.setEditable(false);
      m_jtextarea1.setFont(new Font("SansSerif",Font.PLAIN,12));
      m_jtextarea1.setText("Click to respawn the ball at the mouse.\nUse A and D to flip the flippers.\n\nMade by Michael Moore.");
      JScrollPane jscrollpane1 = new JScrollPane();
      jscrollpane1.setViewportView(m_jtextarea1);
      jscrollpane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
      jscrollpane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      jpanel1.add(jscrollpane1,cc.xywh(1,5,4,1));

      addFillComponents(jpanel1,new int[]{ 2,3,4 },new int[0]);
      return jpanel1;
   }

   /**
    * Initializer
    */
   protected void initializePanel()
   {
      setLayout(new BorderLayout());
      add(createPanel(), BorderLayout.CENTER);
   }


}
